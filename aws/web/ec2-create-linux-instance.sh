#!/bin/bash

# Creating a linux ec2 instance
aws ec2 run-instances \
--image-id "ami-0629230e074c580f2" \
--instance-type "t2.micro"\
--count 1 \
--subnet-id "subnet-014d5a80daa1e1fce"\
--tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=LinuxWeb}]'\
--security-groups-ids "sg-0737151153997efda"\
--key-name "web-key-pair"\
--user-data file://web.txt --profile rohit
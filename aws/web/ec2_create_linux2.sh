#!/bin/bash
aws ec2 run-instances \
--image-id "ami-0567e0d2b4b2169ae" \
--instance-type "t2.micro" \
--count 1 \
--subnet-id "subnet-05ab41e9ef021a424" \
--tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=LinuxServer-Web}]' \
--security-group-ids "sg-02a5432db5b12cd4d" \
--key-name "key-pair-pem" \
--user-data file://web.txt --profile rohit
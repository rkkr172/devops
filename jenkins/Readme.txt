Code build
Code pipeline

#Continuous Integration Tool
CI : jenkins
port - 8080/tcp

Pre-requisite Packages
    - ntp
    - ca-certificates
    - openjdk-11-jdk
    - maven
    - jenkins

Service: systemctl start jenkins

curl -fsSL https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo tee /usr/share/keyrings/jenkins-keyring.asc > /dev/null
echo deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc] https://pkg.jenkins.io/debian-stable binary/ | sudo tee \
/etc/apt/sources.list.d/jenkins.list > /dev/null

mvn --version
source /etc/environment
echo #MAVEN_HOME
echo #JAVA_HOME

#Maven lifecycle:
    - https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html
    - clean:
    - validate - validate the project is correct and all necessary information is available
    - compile - compile the source code of the project
    - test - test the compiled source code using a suitable unit testing framework. These tests should not require the code be packaged or deployed
    - package - take the compiled code and package it in its distributable format, such as a JAR.
    - verify - run any checks on results of integration tests to ensure quality criteria are met
    - install - install the package into the local repository, for use as a dependency in other projects locally
    - deploy - done in the build environment, copies the final package to the remote repository for sharing with other developers and projects
    - release:

mvn verify
mvn clean deploy
mvn clean dependency:copy-dependencies package

#List of Plugins :
    - https://plugins.jenkins.io/conditional-buildstep/
    - https://plugins.jenkins.io/deploy/
    - https://plugins.jenkins.io/envinject/
    - https://plugins.jenkins.io/git-parameter/
    - https://plugins.jenkins.io/github-branch-pr-change-filter/
    - https://plugins.jenkins.io/gitlab-branch-source/
    - https://plugins.jenkins.io/maven-plugin/
    - https://plugins.jenkins.io/maven-invoker-plugin/
    - https://plugins.jenkins.io/publish-over-ssh/

#Manage jenkins
    => Manage Plugins
    => Global Tool Configuration (MAVEN_HOME,JAVA_HOME)

==> job1_clean
https://github.com/keshavkummari/devops.git
    branch: develop
Build Trigger
    => Build whenever a SNAPSHOT dependency is built
Build environment
    => Delete workspace before build starts
Build
    - Rot POM: pom.xml
    - Goals and Actions: clean
Save

==> job2_validate
Build
    - Rot POM: pom.xml
    - Goals and Actions: clean validate
Save

==> job3_compile
Build
    - Rot POM: pom.xml
    - Goals and Actions: clean validate compile
Save

==> job4_test
Build
    - Rot POM: pom.xml
    - Goals and Actions: clean validate compile test
Save

==> job5_package
Build
    - Rot POM: pom.xml
    - Goals and Actions: clean validate compile test package
Save

==> job6_verify
Build
    - Rot POM: pom.xml
    - Goals and Actions: clean validate compile test package verify
Save

==> job7_install
Build
    - Rot POM: pom.xml
    - Goals and Actions: clean validate compile test package verify install
Save

==> job8_deploy
Build
    - Rot POM: pom.xml
    - Goals and Actions: clean validate compile test package verify install deploy
Save

References:
    - https://www.jenkins.io/doc/book/installing/linux/#debianubuntu
    - https://stackoverflow.com/questions/69495517/unable-to-install-jenkins-on-ubuntu-20-04
    - https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html
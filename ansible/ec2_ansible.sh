#!/bin/bash
aws ec2 run-instances \
--image-id "ami-0851b76e8b1bce90b" \
--instance-type "t2.micro" \
--count 1 \
--subnet-id "subnet-0aa432f0a02756ccc" \
--tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=ansibleController}]' \
--security-group-ids "sg-0392527cfc1e4d72d" \
--key-name "pair3" \
--user-data file://ansible_userdata.txt --profile rohit
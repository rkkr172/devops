 
 ## Backup
 sudo cp -pv /etc/ansible/hosts /etc/ansible/hosts_$(date +%F_%R)

 ## Ad-Hoc Commands 
 ansible -i inventory -m ansible.builtin.file -a "dest=/opt/devops.txt mode=600" web
 ansible -i inventory -m ansible.builtin.file -a "dest=/opt/devops.txt mode=600 state=directory owner=rohit group=rohit" web

  ansible web -i inventory -m ansible.builtin.file -a "dest=/tmp/devops2.txt mode=600 state=touch"
  ansible web -i inventory -m ansible.builtin.file -a "dest=/tmp/devops2.txt mode=600 state=touch" --become
  
  ansible web -i inventory -m shell -a "free -m"
  ansible web -i inventory -m shell -a "hostname"
  ansible web -i inventory -m shell -a "systemctl status sshd"

  ## Playbooks 
  ansible-playbook --syntax-check ntp.yml -i inventory
  ansible-playbook --check ntp.yml -i inventory
  ansible-playbook ntp.yml -i inventory

## References:
    https://docs.ansible.com/ansible/latest/user_guide/intro_adhoc.html
    https://docs.ansible.com/ansible/latest/user_guide/playbooks_loops.html
    
Ansible is an open-source software provisioning, configuration management, and application-deployment tool enabling infrastructure as code.
# [ Component of Models ]
1. Application
2. Data
3. Runtime
4. Container Engine
5. Operating System
6. Virtualization
7. Servers
8. Storage
9. Networking

# [ IaaS : Infrastructure as a Service ] https://cloud.google.com/learn/what-is-iaas
- <strong>Customized Component :</strong>
    1. Application
    2. Data
    3. Runtime
    4. Container Engine
    5. Operating System
- <strong>Offer as a service : </strong><br>
    6. Virtualization <br>
    7. Servers <br>
    8. Storage <br>
    9. Networking <br>

# [ CaaS : Cloud as a Service ]
- <strong>Customized Component :</strong>
    1. Application
    2. Data
    3. Runtime
- <strong>Offer as a service : </strong><br>
    4. Container Engine <br>
    5. Operating System <br>
    6. Virtualization <br>
    7. Servers <br>
    8. Storage <br>
    9. Networking <br>

# [ PaaS : Platform as a Service ]
- <b>Customized Component :</b>
    1. Application 
    2. Data
- <b>Offer as a service :</b> <br>
    3. Runtime <br>
    4. Container Engine <br>
    5. Operating System <br>
    6. Virtualization <br>
    7. Servers <br>
    8. Storage <br>
    9. Networking

# [ SaaS : Software as a Service ]
- <b>Offer as a service as below:</b>
    1. Application
    2. Data
    3. Runtime
    4. Container Engine
    5. Operating System
    6. Virtualization
    7. Servers
    8. Storage
    9. Networking
***
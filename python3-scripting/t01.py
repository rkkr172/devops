from cgitb import text

s1 = "Hello World How Are You"
#print(s1)

x,y='Rohit',"26"
#print(f'{x} is {y} years of age!')
#print('{} is {} years of age!'.format(x,y))

print('{} {} {}'.format("Quick","Brown","Fox"))
print('{2} {0} {1}'.format("Quick","Brown","Fox"))
print("{q} - {b} - {f}".format(q="quick",b="Brown",f="Fox"))

r=123.0987654321
print(f"result={r:1.2f}")
print("result={res:1.2f}".format(res=r))